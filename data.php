<?php

////
return [
    'form' => [
        'klass' => [
            'type' => 'input',
            'size' => 3,
            'htmlType' => 'number',
            'placeholder' => 'Класс',
        ],
        'fname' => [
            'type' => 'input',
            'size' => 20,
            'htmlType' => 'text',
            'placeholder' => 'Имя',
        ],
        'sname' => [
            'type' => 'input',
            'size' => 20,
            'htmlType' => 'text',
            'placeholder' => 'Фамилия',
        ],
        'tname' => [
            'type' => 'input',
            'size' => 20,
            'htmlType' => 'text',
            'placeholder' => 'Отчество',
        ],
        'hpdata' => [
            'type' => 'input',
            'size' => 10,
            'htmlType' => 'date',
            'placeholder' => 'День рождения',
        ],
        'grant' => [
            'type' => 'select',
            'placeholder' => 'Датация',
            'choices' => [
                '1' => 'Да',
                '2' => 'Нет',
            ],
        ],

    ],
    'data' => 2
];

?>