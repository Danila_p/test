<?php
$data = include __DIR__ . DIRECTORY_SEPARATOR . 'data.php';

?>
<!DOCTYPE html>
<html>
<head>
    <title>Документ HTML5</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script type="text/javascript" src="js/script.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>


<table id=tl>
    <caption><b>Таблица ученики</b></caption>

    <thead>
    <tr>
        <th>Класс</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Отчество</th>
        <th>Дата рождения</th>
        <th>Датация</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php
    $host="localhost";
    $user="root";
    $pass="root"; //установленный вами пароль
    $db_name="students";
    $link = mysqli_connect($host, $user, $pass, $db_name);
	mysqli_query($link,"SET NAMES 'utf8'");
    $query = "SELECT * FROM Students";
    $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
    if ($result) {
        $rows = mysqli_num_rows($result); // количество полученных строк
        for ($i = 0; $i < $rows; ++$i) {
            $row = mysqli_fetch_row($result);
            echo "<tr>";
            for ($j = 1; $j < 7; ++$j) echo "<td>$row[$j]</td>";
            echo "</tr>";
        }
        mysqli_free_result($result);
    }
    ?>
    <!--
    <tr>

        <td>8</td>
        <td>Скотт</td>
        <td>Пилигримм</td>
        <td>Висарионович</td>
        <td>25.05.1989</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>1</td>
        <td>Николай</td>
        <td>Некрасов</td>
        <td>Алексеевич</td>
        <td>16.02.2011</td>
        <td>Нет</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Андрей</td>
        <td>Тиханов</td>
        <td>Владимирович</td>
        <td>02.03.2004</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Евгения</td>
        <td>Превизенцева</td>
        <td>Ивановна</td>
        <td>18.06.1990</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Александр</td>
        <td>Пушкин</td>
        <td>Сергеевич</td>
        <td>20.04.1999</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Алиса</td>
        <td>Сказкинская</td>
        <td>Иззазеркалья</td>
        <td>09.07.1899</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Данила</td>
        <td>Паньков</td>
        <td>Станиславович</td>
        <td>23.03.1990</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Аня</td>
        <td>Терехова</td>
        <td>Андреевна</td>
        <td>29.09.1989</td>
        <td>Да</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Ёжик</td>
        <td>Туманов</td>
        <td>Лесовский</td>
        <td>02.06.1980</td>
        <td>Нет</td>
    </tr>
    <tr>
        <td>11</td>
        <td>Семён</td>
        <td>Семенов</td>
        <td>Семенович</td>
        <td>24.01.1855</td>
        <td>Да</td>
    </tr>
    -->
    </tbody>
    <tfoot>
    <tr>
        <th colspan=5>Всего учеников</th>
        <th id="vsego"></th>
    </tr>
    </tfoot>
</table>

<script type="text/javascript">
    $(function () {
        var $from = $('#pr_form');
        $from.submit(function (event) {
            event.preventDefault();
            var $form = $(this);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'json',
                success: function (result) {

					if (result['tr']) {  //стирание ошибок и заполнение ячеек
						var $tbody=$('#tbody');
						var $tr=$tbody.append($('<tr/>'));
						
						$.each(result['tr'], function (index) {
                            var tr = this;
                            var $errorsContainer = $('#error_' + index);
                            $errorsContainer.html('');
                            $.each(tr, function () {
                                    var $errorHtml = $('<li/>');
                                    $errorHtml.html('');
                                    $errorsContainer.append($errorHtml);
                                }
                            );
							
							$tr.append($('<td/>').html(this));
                        }
                    ); 
						refresh_sum();
					}
					if (result['errors']) {  //показ ошибок
                    $.each(result['errors'], function (index) {

                            var error = this;
                            var $errorsContainer = $('#error_' + index);
                            $errorsContainer.html('');
                            $.each(error, function () {
                                    var $errorHtml = $('<li/>');
                                    $errorHtml.html(this);
                                    $errorsContainer.append($errorHtml);
                                }
                            );
                        }
                    );}
                }
            });
        });
        });

</script>

<form method="post" name=formochka action=save.php id=pr_form>

    <?php
    foreach ($data['form'] as $key => $field) { ?>

    <?php if ($field['type'] == 'input') { ?>
            <input placeholder="<?php echo $field['placeholder']?>"
                   name="Students[<?php echo  $key?>]"
                   type="<?php echo  $field['htmlType']?>"
                   size="<?php ?>">
    <?php } ?>

    <?php if ($field['type'] == 'select') { ?>
            <select name="Students[<?php echo  $key?>]" >
                <?php foreach ($field['choices'] as $k2 => $opt) {?>

                <option value="<?php echo $opt ?>"><?php echo $opt; ?></option>
                <?php } ?>
            </select>
    <?php } ?>

        <ul class="errors" id="error_<?php echo $key ?>">
        </ul>


    <?php }

?>
    <button type="submit">Добавить</button>
</form>
<div id="result_form"></div> <!-- здесь будет результат -->

<script type="text/javascript">
    refresh_sum();
</script>
</body>
</html>